import os

from django.views import generic
from django.shortcuts import HttpResponseRedirect
from django.conf import settings

from celery import group, chain

from pinned_photos.utils import GetZippedFacebookPhotos
from pinned_photos.tasks import get_image_source_fb_object, get_image_from_source, create_zip_file
from pinned_photos import exception as pe


class IndexView(generic.TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        ctx = super(IndexView, self).get_context_data(**kwargs)
        return ctx

    def get(self, request, *args, **kwargs):
        ctx = self.get_context_data(**kwargs)
        destination = settings.MEDIA_ROOT + '/zips/'

        if os.path.isfile(destination + str(self.request.user.id) + '.zip'):
            ctx['can_download'] = True
            pass

        if request.user.is_authenticated:

            try:
                tagged_image_response_object = GetZippedFacebookPhotos(request.user).save_to_db()
                r = get_image_source_fb_object
                s = get_image_from_source

                #  downloading and saving images to project.
                g = group(r.s(each['id'], request.user.id) | s.s(each['id'], request.user.id)
                          for each in tagged_image_response_object.read_response_as_json()['data'])

                # next is to generate zip and send email.
                k = chain(create_zip_file.s())

                l = chain(g | k)
                l.apply_async()
                # ctx['message'] = 'Download link will been sent to your email.'
                if self.request.GET.get('download') == 'True':
                    if os.path.isfile(destination + str(self.request.user.id) + '.zip'):
                        return HttpResponseRedirect('/media/zips/' + str(self.request.user.id) + '.zip?download_image=true')
            except (pe.FacebookAppDoesNotHavePermissionException, pe.SocialAccountDoesNotExistException) as e:
                ctx['message'] = e.msg

        return self.render_to_response(ctx, **kwargs)

