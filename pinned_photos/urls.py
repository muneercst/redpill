from django.urls import path

from pinned_photos.views import IndexView


urlpatterns = [
    path('', IndexView.as_view(), name='index'),

]
