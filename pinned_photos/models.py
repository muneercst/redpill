import os
import json
from django.db import models
from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import post_delete, pre_delete

User = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')


class FacebookTaggedImageResponse(models.Model):
    user = models.ForeignKey(User, related_name='user_facebook_tagged_images', on_delete=models.CASCADE)
    response = models.TextField('Raw Response')
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    expiration_date = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = "FacebookTaggedImageResponse"
        verbose_name_plural = "FacebookTaggedImageResponses"
        unique_together = ('user', )

    def read_response_as_json(self):
        return json.loads(self.response)

    # def __str__(self):
    #     return self.user

@receiver(pre_delete, sender=FacebookTaggedImageResponse)
def zip_delete(sender , instance, **kwargs):
    destination = settings.MEDIA_ROOT + '/zips/'
    if os.path.isfile(destination + str(instance.user.id) + '.zip'):
        os.remove(destination + str(instance.user.id) + '.zip')
