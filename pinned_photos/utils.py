import os
import json
import zipfile
import requests
import datetime
from allauth.socialaccount.models import SocialAccount, SocialToken, SocialApp
from allauth.socialaccount.providers import registry
from allauth.socialaccount.providers.facebook import provider as fb_provider
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from django.utils import timezone
from pinned_photos import exception as pe
from pinned_photos.models import FacebookTaggedImageResponse

User = get_user_model()

FACEBOOK = 'facebook'
IMAGE_STORAGE_PATH = 'facebook_images/'
ZIP_DIR = 'media/zips/'


def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            print(file, "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
            ziph.write(os.path.relpath(os.path.join(root, file)))


def create_zip_file(path, user_id):
    if not os.path.exists(os.path.dirname(ZIP_DIR)):
        os.makedirs(ZIP_DIR)
    zipf = zipfile.ZipFile(str(user_id) + '.zip', 'w', zipfile.ZIP_DEFLATED)
    zipdir(path, zipf)
    zipf.close()
    os.rename(str(user_id) + '.zip', ZIP_DIR + str(user_id) + '.zip')
    return ZIP_DIR + str(user_id) + '.zip'


def make_get_request(url):
    return requests.get(url)


def read_image_from_url(source_url):
    return requests.get(source_url).content


class GetZippedFacebookPhotos:
    photo_type = 'tagged'

    def __init__(self, user):
        self.user = user
        self._get_fb_social_account()
        self._get_social_token()
        self._get_fb_graph_api_url()

    def _get_fb_social_account(self):
        social_account = SocialAccount.objects.filter(provider=registry.by_id(FACEBOOK).id, user=self.user)
        if not social_account.exists():
            raise pe.SocialAccountDoesNotExistException
        self.social_account = social_account[0]

    def _get_social_token(self):
        self.social_app = get_object_or_404(SocialApp, provider=registry.by_id(FACEBOOK).id)
        self.social_token = SocialToken.objects.get(account=self.social_account, app=self.social_app)

    def _get_fb_graph_api_url(self):
        self.fb_graph_api_url = fb_provider.GRAPH_API_URL

    def _prepare_photo_object_fetch_url(self, type):
        url = self.fb_graph_api_url + '/' + str(self.social_account.uid) + '/' + 'photos?type=' + type
        url += '&access_token=' + str(self.social_token.token)
        return url

    def get_tagged_image_objects_from_facebook(self):
        photo_objects_fetch_url = self._prepare_photo_object_fetch_url(self.photo_type)
        response = make_get_request(photo_objects_fetch_url)

        # if no photos or no permissions
        if not json.loads(response.text)['data']:
            raise pe.FacebookAppDoesNotHavePermissionException
        return response

    def get_image_source_url_from_id(self, id):
        facebook_photo_url = self.fb_graph_api_url + '/' + str(id) + '/?fields=source'
        facebook_photo_url += '&access_token=' + str(self.social_token.token)
        response = json.loads(make_get_request(facebook_photo_url).text)
        return response['source']

    # def download_images_from_response(self):
    #     json_response = json.loads(self.get_tagged_image_objects_from_facebook().text)
    #     source_image_urls = []
    #     for each in json_response['data']:
    #         # downloading each image.
    #         source_image_urls.append((each['id'], self.get_image_source_url_from_id(each['id'])))
    #
    #     img_path = IMAGE_STORAGE_PATH + str(self.user.id) + '/'
    #
    #     if not os.path.exists(os.path.dirname(img_path)):
    #         os.makedirs(img_path)
    #
    #     for each in source_image_urls:
    #         with open(img_path + each[0], 'wb') as f:
    #             f.write(read_image_from_url(each[1]))
    #
    #     create_zip_file(img_path, self.user.id)
    #
    #     return ZIP_DIR + str(self.user.id) + '.zip'

    def save_to_db(self):
        response = self.get_tagged_image_objects_from_facebook()
        tagged_image_response_object, _ = FacebookTaggedImageResponse.objects.get_or_create(user=self.user)
        tagged_image_response_object.response = response.text
        tagged_image_response_object.expiration_date = timezone.now() + datetime.timedelta(minutes=1)
        tagged_image_response_object.save()
        return tagged_image_response_object
