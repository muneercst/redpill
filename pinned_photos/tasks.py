import os
import json
from datetime import datetime, timedelta
from django.contrib.auth import get_user_model
from django.conf import settings
from django.utils import timezone
from celery.schedules import crontab
from celery import task, group, chain
from celery.task import periodic_task

from allauth.socialaccount.providers.facebook import provider as fb_provider
from django.core.mail import EmailMultiAlternatives
from django.template import loader
from conf.celery import app
from pinned_photos.models import FacebookTaggedImageResponse

User = get_user_model()

FB_GRAPH_API_URL = fb_provider.GRAPH_API_URL
IMAGE_STORAGE_PATH = 'facebook_images/'


@app.task
def get_image_source_fb_object(id, user_id):
    from pinned_photos.utils import GetZippedFacebookPhotos, make_get_request
    user = User.objects.get(pk=user_id)
    fb_detail_object = GetZippedFacebookPhotos(user)
    social_token = fb_detail_object.social_token
    facebook_photo_url = FB_GRAPH_API_URL + '/' + str(id) + '/?fields=source'
    facebook_photo_url += '&access_token=' + str(social_token.token)
    response = json.loads(make_get_request(facebook_photo_url).text)
    return response['source']


@app.task
def get_image_from_source(source_url, id, user_id):
    from pinned_photos.utils import read_image_from_url
    img_path = IMAGE_STORAGE_PATH + str(user_id) + '/'
    if not os.path.exists(os.path.dirname(img_path)):
        os.makedirs(img_path)
    with open(img_path + id, 'wb') as f:
        f.write(read_image_from_url(source_url))
    return user_id


@app.task
def create_zip_file(user_id):
    user_id = user_id[0] if type(user_id) == list else user_id
    from pinned_photos.utils import create_zip_file
    img_path = IMAGE_STORAGE_PATH + str(user_id) + '/'
    zip_file_path = create_zip_file(img_path, user_id)
    return zip_file_path


# @app.task
# def send_image_download_email(zip_file_path, to_email):
#     from django.contrib.sites.models import Site
#     site = Site.objects.get(id=settings.SITE_ID)
#     subject, from_email, to = 'ZIP FILE LINK', settings.SERVER_EMAIL, to_email
#     text_content = 'Download link: ' + site.domain + zip_file_path
#     t = loader.get_template('email/zip_link.html')
#     c = {'domain': site.domain, 'zip_file_path': zip_file_path}
#     html_content = t.render(c)
#     msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
#     msg.attach_alternative(html_content, "text/html")
#     msg.send()


@app.task
def final_task(user_id):
    """
    for testing purposes
    """
    from pinned_photos.utils import GetZippedFacebookPhotos
    user = User.objects.get(id=user_id)

    tagged_image_response_object = GetZippedFacebookPhotos(user).save_to_db()

    r = get_image_source_fb_object
    s = get_image_from_source

    #  downloading and saving images to project.
    g = group(r.s(each['id'], user.id) | s.s(each['id'], user.id)
              for each in tagged_image_response_object.read_response_as_json()['data'])

    # next is to generate zip and send email.
    k = chain(create_zip_file.s(),)

    l = chain(g | k)
    l()
@periodic_task(
    run_every=(crontab(minute='*/60')),
    name="periodic_task_test",
    ignore_result=True,
)
def periodic_task_test():
    time_threshold = datetime.now() - timedelta(hours=1)
    FacebookTaggedImageResponse.objects.filter(created__lt=time_threshold).delete()


app.conf.timezone = 'Asia/Kolkata'



# @periodic_task(run_every=crontab(minute='*/1'))
# def delete_old_logs():
#     # Query all the foos in our database
#     photos = FacebookTaggedImageResponse.objects.all()
#
#     # Iterate through them
#     for photo in photos:
#
#         # If the expiration date is bigger than now delete it
#         if photo.expiration_date < timezone.now():
#             photo.delete()
#             # log deletion
#     return "completed deleting photos at {}".format(timezone.now())
