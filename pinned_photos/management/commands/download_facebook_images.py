from django.core.management.base import BaseCommand

from pinned_photos.utils import run_download


class Command(BaseCommand):
    def handle(self, *args, **options):
        run_download()