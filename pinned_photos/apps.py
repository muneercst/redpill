from django.apps import AppConfig


class PinnedPhotosConfig(AppConfig):
    name = 'pinned_photos'
