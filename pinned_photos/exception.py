

class SocialAccountDoesNotExistException(BaseException):
    msg = 'Social account does not exist'


class FacebookAppDoesNotHavePermissionException(BaseException):
    msg = 'Account does not give permission to download photos or no photos to download'
