# facebook_redpill
Unplug from facebook

## Requirements
- Python 3.6
- Django 2.0.3

Extra Details:

1. Please install RabbitMQ using command 'sudo apt-get install rabbitmq-server'.
2. Run worker using command 'celery worker -A conf -l info'
3. A worker is required in-order to send the zip file link to email.