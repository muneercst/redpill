"""
Local developement settings
"""

from conf.settings.base_settings import *


SECRET_KEY = 'vs-@jbb6fdp9!znr(nm_ik^2rn^vj^^rm^8dfq66+(=!vn1ao$'


SOCIALACCOUNT_PROVIDERS = {
    'facebook': {
        'METHOD': 'oauth2',
        'SCOPE': ['email', 'public_profile', 'user_friends','user_photos'],
        'AUTH_PARAMS': {'auth_type': 'reauthenticate'},
        'INIT_PARAMS': {'cookie': True},
        'FIELDS': [
            'id',
            'email',
            'name',
            'first_name',
            'last_name',
            'verified',
            'locale',
            'timezone',
            'link',
            'gender',
            'updated_time',
        ],
        'EXCHANGE_TOKEN': True,
        'LOCALE_FUNC': 'path.to.callable',
        'VERIFIED_EMAIL': False,
        'VERSION': 'v2.12',
    }
}

# All auth settings
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_EMAIL_VERIFICATION = "none"
ACCOUNT_LOGIN_ON_EMAIL_CONFIRMATION = True
ACCOUNT_AUTHENTICATION_METHOD = 'email'

SITE_ID = 1

EMAIL_BACKEND = 'sgbackend.SendGridBackend'
SENDGRID_API_KEY = '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'

SERVER_EMAIL = 'rajeshkrish.sayone@gmail.com'



DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(PROJECT_DIR, 'yourdatabasename.db'),
    }
}
