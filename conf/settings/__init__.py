import os

env = os.getenv('REDPILL_ENV', None)

if env == 'production':
    from conf.settings.production import *
elif env == 'staging':
    from conf.settings.staging import *
else:
    from conf.settings.local import *