"""
Production ready settings
"""

from conf.settings.base_settings import *


ALLOWED_HOSTS = ['40756782.ngrok.io']


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'redpill',
        'USER': 'cst123',
        'PASSWORD': 'Classy@$e',
        'HOST': '',
        'PORT': '',
    }
}
